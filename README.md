# Mumba Sourced type definition

Typescript type definition for [mateodelnorte/sourced](https://github.com/mateodelnorte/sourced) and [mateodelnorte/sourced-repo-mongo](https://github.com/mateodelnorte/sourced-repo-mongo).

## Installation

```sh
$ npm install --save mumba-typedef-sourced
$ typings install sourced=npm:mumba-typedef-sourced --save --global
```

## People

The original author of _Mumba TypeDef Sourced_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/typedef-sourced/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.


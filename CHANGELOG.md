# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.1.3] 19 Sep 2016
## [0.1.2] 19 Sep 2016
- Added `entityType` property and balance of methods to `sourced-repo-mongo` definition.

## [0.1.1] 13 Sep 2016
- Fixed up incorrect references and instruction in README.

## [0.1.0] 12 Sep 2016
- Initial release.
